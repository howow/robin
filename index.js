'use strict'

/* eslint-disable no-unused-vars */
import {
  F, T, __, identity as I, o, add, addIndex, adjust, all, allPass,
  always, and, andThen, any, anyPass, ap, aperture, append, apply,
  applySpec, applyTo, ascend, assoc, assocPath, binary, bind, both, call, chain,
  clamp, clone, comparator, complement, compose, composeWith, concat,
  cond, construct, constructN, converge, countBy, curry, curryN, dec,
  defaultTo, descend, difference, differenceWith, dissoc, dissocPath, divide,
  drop, dropLast, dropLastWhile, dropRepeats, dropRepeatsWith, dropWhile,
  either, empty, endsWith, eqBy, eqProps, equals, evolve,
  filter, find, findIndex, findLast, findLastIndex, flatten, flip,
  forEach, forEachObjIndexed, fromPairs, groupBy, groupWith, gt, gte,
  has, hasIn, hasPath, head, identical, ifElse, inc, includes, indexBy, indexOf,
  init, innerJoin, insert, insertAll, intersection, intersperse, into, invert, invertObj,
  invoker, is, isEmpty, isNil, join, juxt, keys, keysIn, last, lastIndexOf,
  length, lens, lensIndex, lensPath, lensProp, lift, liftN, lt, lte,
  map, mapAccum, mapAccumRight, mapObjIndexed, match, mathMod, max, maxBy,
  mean, median, memoizeWith, mergeAll, mergeDeepLeft, mergeDeepRight,
  mergeDeepWith, mergeDeepWithKey, mergeLeft, mergeRight, mergeWith,
  mergeWithKey, min, minBy, modulo, move, multiply,
  nAry, negate, none, not, nth, nthArg, objOf, of, omit, once, or, otherwise,
  over, pair, partial, partialRight, partition, path, pathEq, pathOr,
  pathSatisfies, paths, pick, pickAll, pickBy, pipe, pipeWith, pluck, prepend,
  product, project, prop, propEq, propIs, propOr, propSatisfies, props,
  range, reduce, reduceBy, reduceRight, reduceWhile, reduced, reject, remove,
  repeat, replace, reverse, scan, sequence, set, slice, sort, sortBy, sortWith,
  split, splitAt, splitEvery, splitWhen, startsWith, subtract,
  sum, symmetricDifference, symmetricDifferenceWith, tail,
  take, takeLast, takeLastWhile, takeWhile, tap, test, thunkify, times,
  toLower, toPairs, toPairsIn, toString, toUpper, transduce, transpose, traverse,
  trim, tryCatch, type, unapply, unary, uncurryN, unfold,
  union, unionWith, uniq, uniqBy, uniqWith, unless, unnest, until,
  update, useWith, values, valuesIn, view, when, where, whereEq, without,
  xor, xprod, zip, zipObj, zipWith
} from 'ramda'

export { I }

export * from 'ramda'

/* eslint-enable no-unused-vars */

/** Promise */
const tapPromise = curryN(2, (fn, value) => resolvePromise(value).then(fn).then(() => value))

const mapSerier = (fn) => reduce((acc, value) => {
  return acc.then((x) => fn(value).then(flip(append)(x)))
}, resolvePromise([]))

const allPromise = curryN(1, bind(Promise.all, Promise))

const allThen = curryN(2, useWith(call, [andThen, allPromise]))

const resolvePromise = curryN(1, bind(Promise.resolve, Promise))

const rejectPromise = curryN(1, bind(Promise.reject, Promise))

const delayPromise = (timeout) => new Promise((resolve) => setTimeout(resolve, timeout))

const pipeWithPromise = pipeWith((fn, result) => resolvePromise(result).then(fn))

const composeWithPromise = composeWith((fn, result) => resolvePromise(result).then(fn))

const valueOrKey = (keysMap) => (key) => has(key, keysMap) ? keysMap[key] : key

const renameKeysWith = curry((fn, obj) =>
  pipe(toPairs, map(over(lensIndex(0), fn)), fromPairs)(obj)
)

const renameKeys = curry((keysMap, obj) =>
  renameKeysWith(valueOrKey(keysMap), obj)
)

/** const */
const stubUndefined = always(void 0)

const stubNull = always(null)

/** logic */
const length1 = o(equals(1), length)

const length2 = o(equals(2), length)

const length3 = o(equals(3), length)

const length4 = o(equals(4), length)

const length5 = o(equals(5), length)

const length6 = o(equals(6), length)

const length7 = o(equals(7), length)

const length8 = o(equals(8), length)

const length9 = o(equals(9), length)

const isNotEmpty = complement(isEmpty)

const isNilOrEmpty = curryN(1, either(isNil, isEmpty))

const isString = curryN(1, pipe(type, identical('String')))

const notEqual = complement(equals)

export {
  isNilOrEmpty, isNotEmpty, isString, notEqual,
  tapPromise, allPromise, resolvePromise, rejectPromise, mapSerier,
  pipeWithPromise, composeWithPromise, allThen, delayPromise,
  renameKeys,
  stubUndefined, stubNull,
  renameKeysWith
}

const print = bind(console.log, console)
const log = curryN(3, print)(__, '~>', __)
const llog = curryN(2, (title, x) => print(`---v: ${title} :v---\n`, isString(x) ? x : JSON.stringify(x, null, 4), `\n---^: ${title} :^---`))
const xlog = converge(I, [nthArg(1), log])
const xllog = converge(I, [nthArg(1), llog])
const debug = xllog('debug')

const num = (x) => +x || 0
const int = (x) => +x || 0
const Z = (x) => Math.floor(+x || 0)
const str = (x) => x == null ? '' : '' + x
const emptyObject = {}
const emptyString = ''
const emptyArray = []
const alwaysNew = thunkify(clone)
const alwaysEmptyObject = alwaysNew(emptyObject)
const alwaysEmptyString = alwaysNew(emptyString)
const alwaysEmptyArray = alwaysNew(emptyArray)
const isZero = equals(0)
const isArray = Array.isArray
const noop = always(stubUndefined())

const ignoreError = o(F, print)

const trimZWSP = replace(/[\u200B-\u200D\uFEFF]/g, '')

const hasValue = cond([
  [isNil, F],
  [isEmpty, F],
  [isString, complement(o(isEmpty, trim))],
  [T, T]
])

const lowerHead = replace(/^./, toLower)
const upperHead = replace(/^./, toUpper)
const capitalize = compose(upperHead, toLower)
const deburr = split(/[^0-9a-zA-Z]/)
const pascalCase = compose(join(''), map(capitalize), deburr)
const camelCase = compose(lowerHead, pascalCase)

/** (padString,targetLength)=>string */
const padStart = flip(invoker(2, 'padStart'))

/** (padString,targetLength)=>string */
const padEnd = flip(invoker(2, 'padEnd'))
/**
 * 將參數轉成陣列
 * list(a,b,c) \\ => [a,b,c]
 */
const list = unapply(I)

/** map */
const mapIndexed = addIndex(map)

/** 去除前後空白, 含無長度字元 */
const cleanString = o(trimZWSP, trim)

/**  */
/** 'prefix', env */
const envConfig = curryN(2, compose(
  renameKeysWith(camelCase),
  converge(renameKeysWith, [
    unary(replace(__, '')),
    useWith(pickBy, [compose(flip, test), I])
  ])
))

const newError = thunkify((e) => { throw new Error(`env must has ${e}`) })

const env = curryN(2, (key, ev) => either(prop(key), newError(key))(ev))

const envKey = memoizeWith(I, env)

const begin = bind(console.time, console)

const end = bind(console.timeEnd, console)

const measure = curryN(2, (key, fn) => {
  const p = thunkify(begin)(key)
  const q = thunkify(end)(key)

  return (...args) => {
    p()
    const result = fn(...args)
    q()
    return result
  }
})

/**
 * 民國轉西元
 * regExp => array
 * ex: /^(\d+)年(\d+)月(\d+)日$/ => [yyyy,MM,DD]
*/
const minguoToCE = curryN(2, pipe(
  match,
  tail,
  take(3),
  mapIndexed((v, i) => i > 0 ? padStart('0', 2, str(+v || 1)) : str((+v || 0) + 1911))
))

const jsonOr = curryN(2, (val, str) => {
  try {
    return JSON.parse(str)
  } catch (e) {
    print('parse error:', '`' + str + '`', 'not JSON')
    return val
  }
})

const jsonSafe = curryN(1, (str) => {
  try {
    return JSON.parse(str)
  } catch (e) {
    return str
  }
})

/**
 * return a test and match function pairs array
 * use cond function, if test rx is true then return match last group
 *
 * @param {RegExp} rx
 * @returns array
 */
const condMatch = rx => [test(rx), o(last, match(rx))]

const omitEmptyValue = pickBy(hasValue)

const nativeFloor = bind(Math.floor, Math)

const nativeRandom = bind(Math.random, Math)

const random = curryN(2, (lower, upper) => lower + nativeFloor(nativeRandom() * (upper - lower + 1)))

const shuffler = curry((random, list) => {
  let i = -1
  let len = list.length
  let position
  const result = []
  while (++i < len) {
    position = Math.floor((i + 1) * random())
    result[i] = result[position]
    result[position] = list[i]
  }
  return result
})

export {
  shuffler,
  random,
  condMatch,
  length1,
  length2,
  length3,
  length4,
  length5,
  length6,
  length7,
  length8,
  length9,
  omitEmptyValue,
  jsonSafe,
  jsonOr,
  minguoToCE,
  padStart,
  padEnd,
  measure,
  envKey,
  envConfig,
  cleanString,
  mapIndexed,
  list,
  camelCase,
  hasValue,
  trimZWSP,
  ignoreError,
  alwaysNew,
  alwaysEmptyObject,
  alwaysEmptyString,
  alwaysEmptyArray,
  noop,
  isZero,
  isArray,
  int,
  Z,
  num,
  str,
  log,
  llog,
  xlog,
  xllog,
  print,
  debug
}
